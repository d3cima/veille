<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 6/5/18
 * Time: 2:04 PM
 */
namespace App\EventListener;

use App\Service\PostManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Entity\Post;

class PostProcessing
{
    /**
     * @var PostManager
     */
    private $postManager;

    /**
     * PostProcessing constructor.
     *
     * @param PostManager $postManager
     */
    public function __construct(PostManager $postManager)
    {
        $this->postManager = $postManager;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ( ! $entity instanceof Post) {
            return;
        }
        $this->postManager->postUrl($entity->getUrl());

    }

}