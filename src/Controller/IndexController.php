<?php
namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Service\PostManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template("index.html.twig")
     */
    public function index(
        Request $request,
        PostManager $posts,
        PostRepository $postRepository
    ) {
        $search = (object)[
            "url" => $request->query->has("qp")
                ? $request->query->get("qp") : "",
        ];
        $form = $this->createFormBuilder($search, [
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'csrf_token_id'   => 'task_item',
        ])
            ->add("url", UrlType::class)
            ->getForm()
        ;
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $posts->postUrl($search->url);
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            return $this->redirectToRoute("index");
        }
        return [
            "form" => $form->createView(),
            "last" => $postRepository->findLastPosts(),
        ];
    }

    public function topCategories($max = 5, CategoryRepository $repository)
    {
        return $this->render('index/top.html.twig',
            ["categories" => $repository->getAllOrderedByUsageAsArray($max)]);
    }
}
