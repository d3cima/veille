<?php
/**
 * Created by PhpStorm.
 * User: decima
 * Date: 03/06/18
 * Time: 13:56
 */

namespace App\Service;


use App\Entity\Category;
use App\Repository\CategoryRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;

class CategoryManager
{
    private $repository;
    private $entityManager;

    public function __construct(CategoryRepository $repository, EntityManagerInterface $entityManager)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
    }

    public function findOrCreate($category)
    {
        $slug = ((new Slugify())->slugify($category));
        $found = $this->repository->findOneBy(["name" => $category]);
        if (!$found) {
            $i = 1;
            do {
                if (!$this->repository->findOneBy(["slug" => "$slug-$i"])) {
                    $entity = new Category($category, "$slug-$i");
                    $this->entityManager->persist($entity);
                    $this->entityManager->flush();
                    return $entity;
                }
                $i++;
            } while (true);
        }
        return $found;

    }
}