<?php
/**
 * Created by PhpStorm.
 * User: decima
 * Date: 03/06/18
 * Time: 13:48
 */
namespace App\Service;

use App\Entity\Post;
use Doctrine\ORM\EntityManager;
use Embed\Adapters\Adapter;
use Embed\Embed;

class PostManager
{
    private $categoryManager;

    public function __construct(CategoryManager $categories)
    {
        $this->categoryManager = $categories;
    }

    public function postUrl($url, Post $post = null)
    {
        $result = Embed::create($url);
        if ( ! $post) {
            $post = new Post();
        }
        $post->setTitle($result->getTitle());
        $post->setDescription($result->getDescription());
        $post->setUrl($url);
        $post->setContent($this->parseFields($result));
        //$this->defineCategories($post, $result);
        return $post;
    }

    private function defineCategories(Post &$post, Adapter $adapter)
    {
        if (\is_array($adapter->tags)) {
            foreach (
                array_unique(array_map("strtolower", $adapter->tags)) as $tag
            ) {
                $post->addCategory($this->categoryManager->findOrCreate($tag));
            }
        }
    }

    private function parseFields(Adapter $adapter)
    {
        $fields = [
            "url",
            "type",
            "images",
            "image",
            "imageWidth",
            "imageHeight",
            "code",
            "width",
            "height",
            "aspectRatio",
            "authorName",
            "authorUrl",
            "providerName",
            "providerUrl",
            "providerIcons",
            "providerIcon",
            "publishedDate",
            "license",
            "linkedData",
            "feeds",
        ];
        return array_reduce($fields, function ($carry, $item) use ($adapter) {
            $carry[$item] = $adapter->{$item};
            return $carry;
        }, []);
    }
}