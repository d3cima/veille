<?php
namespace App\Repository;

use App\Entity\Category;
use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit
 *         = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function getAllOrderedByUsage($hydration = null, $limit = null)
    {
        $qb = $this->createQueryBuilder("c");
        $query = $qb->select("c")
            ->InnerJoin("c.posts", "p")
            ->select("c.id, c.name,c.slug, COUNT(p.id) AS counter")
            ->orderBy("counter", "DESC")
            ->groupBy("c.name")
        ;
        if ($limit !== null) {
            $query = $query->setMaxResults($limit)
                ->setFirstResult(0)
            ;
        }
        return $query->getQuery()->execute(null, $hydration);
    }

    public function getAllOrderedByUsageAsArray($limit = null)
    {
        return $this->getAllOrderedByUsage(Query::HYDRATE_OBJECT, $limit);
    }
}
