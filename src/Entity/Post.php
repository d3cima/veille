<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 * @ApiResource()
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="json_array")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdOn;
    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="posts", cascade={"persist","remove"})
     * @ApiSubresource()
     */
    private $categories;

    public function __construct()
    {
        $this->content = new \stdClass();
        $this->categories = new ArrayCollection();
        $this->createdOn = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories): void
    {
        $this->categories = $categories;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $final = "";
        $obj = parse_url($url);
        if (isset($obj["scheme"])) {
            $final .= $obj["scheme"]."://";
        } else {
            $final .= "http://";
        }
        $hasUser = false;
        if (isset($obj["user"])) {
            $final .= $obj["user"];
            $hasUser = true;
        }
        if (isset($obj["pass"])) {
            $final .= ":".$obj["pass"];
            $hasUser = true;
        }
        if ($hasUser) {
            $final .= "@";
        }
    if (isset($obj["host"])) {
        $final .= $obj["host"];
    }
    if (isset($obj["port"])) {
        $final .= ":".$obj["port"];
    }
    if (isset($obj["path"])) {
        $final .= "/".str_replace("%2F", "/", urlencode($obj["path"]));
    }
    if (isset($obj["query"])) {
            $final .= "?".str_replace(["%26","%3D"], ["&","="],urlencode($obj["query"]));
    }
    if (isset($obj["fragment"])) {
        $final .= "#".urlencode($obj["fragment"]);
    }
   
     $this->url = $final;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content): self
    {
        $this->content = $content;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function addCategory(Category $category)
    {
        if (!$this->categories->contains($category))
            $this->categories->add($category);
    }

    public function getCreatedOn()
    {
        return $this->createdOn;
    }
}
