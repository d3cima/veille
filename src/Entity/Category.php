<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ApiResource()
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;
    /**
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="categories")
     * @ApiSubresource()
     */
    private $posts;

    public function __construct($name = null, $slug = null)
    {
        $this->setName($name);
        $this->setSlug($slug);

    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return mb_convert_case($this->name, MB_CASE_TITLE, "UTF-8");
    }

    public function setName(string $name): self
    {
        $this->name = mb_convert_case($name, MB_CASE_LOWER, "UTF-8");

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }


    public function __toString()
    {
        return $this->name;
    }

}
